﻿namespace PipelineLauncher.Stages
{
    public interface IStageSetup
    {
        IStage Current { get; }
    }
}